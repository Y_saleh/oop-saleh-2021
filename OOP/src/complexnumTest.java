
public class complexnumTest {
	public static void main(String[] args) {
		
		Complexnum k1 = new Complexnum(10, 10); 
		Complexnum k2 = new Complexnum(1,1); 

		Complexnum k3 = Complexnum.mult(k1, k2); //Class method
		Complexnum k4 = k1.mult2(k2); // Object Method
		System.out.printf(k3.toString() + " In Class Method");
		System.out.printf(k4.toString() + " In Object Method");

	}
}
