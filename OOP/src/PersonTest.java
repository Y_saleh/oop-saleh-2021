

public class PersonTest {

	public static void main(String[] args) {

		Date d = new Date(8, 5, 2021);
		Date d1 = new Date(12, 12, 1999);
		Date d2 = new Date(1, 9, 1985);

		Person p  = new Person("Mark", d);
		Person p1 = new Person("Max", d1);
		Person p2 = new Person("Bob", d2);

		System.out.println(p.toString());
		System.out.println(p1.toString());
		System.out.println(p2.toString());

		System.out.println("Counter: "+ p2.counter);
		System.out.println("StaticCounter: "+ p2.staticCounter);

	}

}
