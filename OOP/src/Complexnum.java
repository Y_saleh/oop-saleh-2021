
public class Complexnum {

	private double real;
	private double imaginary;

	public Complexnum() {
		this.real = 0.0;
		this.imaginary = 0.0;

	}

	public Complexnum(double real, double imaginary) {
		this.real = real;
		this.imaginary = imaginary;
	}

	public double getReal() {
		return real;
	}

	public double getImaginary() {
		return imaginary;
	}

	public void setReal(double real) {
		this.real = real;
	}

	public void setImaginary(double imaginary) {
		this.imaginary = imaginary;
	}

	@Override
	public String toString() { // {Complex Number: 1.0 + j 15.0}

		return "[Complex Number: " + this.real + " + j " + this.imaginary + " ]";
	}

	public boolean equals(Object obj) {
		// if (obj == null)
		// return false;

		if (obj instanceof Complexnum) {
			Complexnum c = (Complexnum) obj;
			if (this.real == c.getReal() && this.imaginary == c.getImaginary())
				return true;
			else
				return false;
		}
		return false;
	}

	public static Complexnum mult(Complexnum a, Complexnum b) {
		double resultreal = ((a.getReal() * b.getReal()) - (a.getImaginary() * b.getImaginary()));
		double resultimag = ((a.getReal() * b.getImaginary()) + (a.getImaginary() * b.getReal()));

		return new Complexnum(resultreal, resultimag);

	}

	public Complexnum mult2(Complexnum c) {

		if (c == null) {
			return new Complexnum();
		}
		return new Complexnum(this.real * c.getReal() - this.imaginary * c.getImaginary(),
				this.real * c.getImaginary() + this.imaginary * c.getReal());
	}
}
