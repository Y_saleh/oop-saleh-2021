
public class Person {

	private String name;
	private Date birthday;
	public int counter = 0;
	public static int staticCounter = 0;
	
	public Person() {
		this.name = "Unknown";
		this.birthday = null;
	}

	public Person(String name, Date birthday) {
		this.name = name;
		this.birthday = birthday;
		counter++;
		staticCounter++;
	}

	public String getName() {
		return name;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String toString() { // {Name: Max, Alter: 18}

		return "[ Name: " + this.name + " , Birthday: " + this.birthday + " ]";
	}

	public boolean equals(Object obj) {

		// if (obj == null)
		// return false;

		if (obj instanceof Person) {
			Person p = (Person) obj;
			if (this.name.equals(p.getName()) && this.birthday == p.getBirthday())
				return true;
			else
				return false;
		}
		return false;
	}

}
