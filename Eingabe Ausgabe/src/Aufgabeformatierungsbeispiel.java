
public class Aufgabeformatierungsbeispiel {

	public static void main(String[] args) {
		
		int iZahl = 123;
/*	
		System.out.println(iZahl);
		System.out.printf("%d%n", iZahl);
		System.out.printf("%5d%n", iZahl);
		System.out.printf("%-5d%n", iZahl);
		System.out.printf("%+-5d%n", iZahl);
*/
		double fZahl =1234.56789;
		
/*
		String fFormat   = "|%f|%n";
		
		//fFormat   = "|%0010.3f|%n";
		
		final String FFormat   = "|%010.3f|%n";
		
		System.out.printf("|%.2f|%n", fZahl);
		
		System.out.printf("|%10.2f|%n", fZahl);
		
		System.out.printf(fFormat, fZahl);

		System.out.printf(fFormat, fZahl);
*/
/*
		System.out.printf("   **   %n");
		System.out.printf("*      *%n");
		//System.out.printf("      %n");
		System.out.printf("*      *%n");
		System.out.printf("   **   %n");
*/
		
		String strF = "-20 -10 0 20 30";
		String strC = "-28.89 -23.33 -17.78 -6.67 -1.11";

		System.out.printf("Fahrenheit  |  Celsius%n");
		System.out.printf("----------------------%n");
		System.out.printf("%-12d|%10.2f%n", -20, -28.88889);
		System.out.printf("%-12d|%10.2f%n", -10, -23.33333);
		System.out.printf("%-12d|%10.2f%n", 0, -17.7778);
		System.out.printf("%-12d|%10.2f%n", 20, -6.66667);
		System.out.printf("%-12d|%10.2f%n", 30, -1.111111);


	}

}
