
public class Feiertag extends Date {

	private String name; 
	
	
	public Feiertag () {
//		setName("Unbekannt");
//		setAlter(0);
		super();
		this.name = "Kein Feiertag";
	}
	
	public Feiertag (int day, int month, int year, String name) {
		super(day, month, year);
		setName(name);
	
	}

	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name; 
	}
	
	@Override
	public String toString() {

		return " " + getDay() + " . " + getMonth() + " . " + getYear() + " (" + this.name + " )" ;
	}

}
