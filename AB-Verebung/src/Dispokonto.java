
public class Dispokonto extends Bankkonto{
	
	protected double dispokredit;
	
	public Dispokonto() {
		super();
		setDispokredit(0);
	}
	
	public Dispokonto(String inhaber, double kontostand, double dispokredit) {
		super(inhaber, kontostand);
		setDispokredit(dispokredit);
	}

	public double getDispokredit() {
		return this.dispokredit;
	}

	public void setDispokredit(double dispokredit) {
		this.dispokredit = dispokredit;
	}
	
	@Override
	public double auszahlen (double betrag) {
		double ausgezahlterbetrag = 0.0;
		if (getKontostand() + getDispokredit() - betrag < 0) {
			ausgezahlterbetrag = getKontostand() + getDispokredit();
		} else {
			ausgezahlterbetrag = betrag;
		}
		setKontostand(getKontostand() - ausgezahlterbetrag);
		return ausgezahlterbetrag;
	}
	
	@Override
	public String toString() {
		return String.format("[ %s, Dispokredit: %.2f ]", super.toString(), getDispokredit());
	}
}
