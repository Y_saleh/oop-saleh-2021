import java.util.Scanner;

public class Bankkonto {

	protected String inhaber;
	protected long kontonummer;
	protected double kontostand;
	protected static long kontoCnt = 0;

	public Bankkonto() {
		this.inhaber     = "Unbekannt";
		this.kontostand  = 0.0;
		kontonummer = kontoCnt++;
	}
	public Bankkonto (String inhaber, double kontostand) {
		this.inhaber     = inhaber;
		this.kontonummer = kontonummer;
		this.kontostand  = kontostand;
		this.kontonummer = kontoCnt++;

	}
	public String getInhaber() {
		return inhaber;
	}
	public long getKontonummer() {
		return kontonummer;
	}
	public double getKontostand() {
		return kontostand;
	}
	public void setInhaber(String inhaber) {
		this.inhaber = inhaber;
	}
	public void setKontonummer(long kontonummer) {
		this.kontonummer = kontonummer;
	}
	public void setKontostand(double kontostand) {
		this.kontostand = kontostand;
	}
	
	public void einzahlen (double betrag) {		
		System.out.println("Sie hatten: " + this.kontostand + " \u20ac." + "\n\nSie haben: " + betrag + " \u20ac Eingezahlt" );
		this.kontostand += betrag; 
	}
	
	public double auszahlen (double ausgezahlterbetrag) {

		System.out.println("Sie hatten: " + this.kontostand + " \u20ac." + "\n\nSie haben: " + ausgezahlterbetrag + " \u20ac Ausgezahlt" );
		this.kontostand -= ausgezahlterbetrag;
		return ausgezahlterbetrag;
	}
	
	@Override
	public String toString() {
		return	String.format("[Inhaber des Konto: %s\t|| KontoNummer: %d\t|| Kontostand: %.2f \u20ac\t", getInhaber(), getKontonummer(), getKontostand());
	}
}
