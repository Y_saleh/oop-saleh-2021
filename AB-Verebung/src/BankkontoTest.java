
import java.util.Scanner;

public class BankkontoTest {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		int option = 0;
		String[] options = { "", "Auszahlen", "Einzahlen", "Konto�bersicht" };

		Bankkonto user1 = new Bankkonto();
		Bankkonto user2 = new Bankkonto("Yazan", 1000.0);

		System.out.printf("Verf�gbare Konto:\n\n");
		System.out.println(user1);
		System.out.println(user2);

		for (int i = 1; i < options.length; i++) {
			System.out.println("\nF�r {" + options[i] + "} Dr�cken Sie [" + i + "]");
		}
		System.out.println("\nIhre Option:");

		option = myScanner.nextInt();
		System.out.printf("Sie Haben %d Ausgew�hlt", option);

		switch (option) {
		case 1:

			System.out.println("\nWieviel Geld soll ausgezahlt werden?");
			double betrag = myScanner.nextDouble();
			user2.auszahlen(betrag);
			System.out.printf("\nAkutalle Zustand: %.2f\u20ac", user2.kontostand);

			break;
		case 2:
			System.out.println("\nWieviel Geld soll eingezahlt werden?");
			double eingezahlterbetrag = myScanner.nextDouble();
			double betrag1 = 0;
			betrag1 += eingezahlterbetrag;
			user2.einzahlen(betrag1);
			System.out.printf("\nAkutalle Zustand: %.2f\u20ac", user2.kontostand);
			break;
		case 3:
			System.out.printf("\nAkutalle Zustand: %.2f\u20ac", user2.kontostand);
			break;
		}

		System.out.println("\n\nVielen Dank! Bis Sp�ter");

	}
}
