
public class DispokontoTest {

	public static void main(String[] args) {
		Dispokonto user1 = new Dispokonto("Yazan", 60.0, 100.0);
		Dispokonto user2 = new Dispokonto("Mabu", -40.0, 100.0);


		System.out.printf("paying out %.2f, account is: %s%n", user1.auszahlen(170), user2);
		System.out.printf("\npaying out %.2f, account is: %s%n", user2.auszahlen(50), user2);

		Bankkonto user3 = new Bankkonto("Lina", 100.0);
		Bankkonto user4 = new Bankkonto("Alexander", 100.0);

		Bankkonto[] kontos = { user1, user2, user3, user4 };

		for (int i = 0; i < kontos.length; i++) {
			System.out.println(kontos[i]+"\n");
		}
	}

}
