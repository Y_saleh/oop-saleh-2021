
public class zweiarray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// folgende Matrix erzeugen:
        // | 1 2 3 |
        // | 4 5 6 |
        
        int[][] intMatrix = new int[2][3];
        // ergibt
        // | 0 0 0 |
        // | 0 0 0 |
        
        intMatrix[0][0] = 1;// 1. Zeile: 1 0 0
        intMatrix[0][1] = 2;// 1. Zeile: 1 2 0
        intMatrix[0][2] = 3;// 1. Zeile: 1 2 3
        intMatrix[1][0] = 4;// 2. Zeile: 4 0 0
        intMatrix[1][1] = 5;// 2. Zeile: 4 5 0
        intMatrix[1][2] = 6;// 2. Zeile: 4 5 6
        
        printmatrix(intMatrix);
	}
        
        public static void printmatrix(int[][] intMatrix) {
        for(int zeile = 0; zeile < intMatrix.length; zeile++) {
        // length: Groesse der 1. Dimension
                for(int spalte = 0; spalte < intMatrix[0].length; spalte++)
                // [0].length: Groesse des 1. Elements der 2. Dimension
                        System.out.print(intMatrix[zeile][spalte] + " ");        
                System.out.println("");
        }

        }

}
