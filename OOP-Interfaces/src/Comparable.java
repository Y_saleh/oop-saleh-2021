
public interface Comparable<Datum> {

	public static final int o = 0;
	public int compareTo(Datum o);
}
