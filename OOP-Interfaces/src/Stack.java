
public class Stack implements IntStack {

	public static int arrayLen = 10;
	private int[] memory;
	private int lastIndex;

	public Stack() {
		this.memory = new int[arrayLen];
		this.lastIndex = -1;
	}

	@Override
	public void push(int x) {

		if (this.lastIndex >= memory.length - 1) {
			
			int originalLength = this.memory.length;
			int[] copy = new int [this.memory.length + arrayLen];
			
				for (int i = 0; i< originalLength; i++) {
					copy[i] = this.memory[i];
				}
 
				this.memory = copy;
		}
		
		this.memory[++this.lastIndex] = x;

	}

	@Override
	public int pop() {
		if (this.lastIndex < 0) {
			return -1;
		}
		return this.memory[this.lastIndex--];
	}
	
	public int getLength() {
		return this.memory.length;
	}

}
