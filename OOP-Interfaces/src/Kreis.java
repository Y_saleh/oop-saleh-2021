
public class Kreis extends GeoObject{

	private double radius;
	
	public Kreis(double x, double y, double radius) {
		super(x, y);
		this.radius = radius;
	}
	
	
	public double getRadius() {
		return radius;
	}


	public void setRadius(double radius) {
		this.radius = radius;
	}


	@Override
	public double determineArea() {
		double area = PI * getRadius() * getRadius(); 
		return area;
	}


	@Override
	public String toString() {
		return "Kreis [radius=" + radius + "]";
	}

}
