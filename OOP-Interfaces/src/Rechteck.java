
public class Rechteck extends GeoObject {
	
	private double hohe;
	private double bretie;

	public Rechteck(double x, double y, double hohe, double bretie) {
		super(x, y);
		this.hohe = hohe;
		this.bretie = bretie;

	}

	public double getHohe() {
		return hohe;
	}

	public void setHohe(double hohe) {
		this.hohe = hohe;
	}

	public double getBretie() {
		return bretie;
	}

	public void setBretie(double bretie) {
		this.bretie = bretie;
	}
	

	@Override
	public double determineArea() {
		double area = getHohe() * getBretie();
 		return area;
	}

	@Override
	public String toString() {
		return "Rechteck [hohe=" + hohe + ", bretie=" + bretie + "]";
	}


}
