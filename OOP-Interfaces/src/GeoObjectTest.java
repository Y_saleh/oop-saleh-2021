
public class GeoObjectTest {

	public static void main(String[] args) {

		Rechteck r1 = new Rechteck(0,0,5,5);
		Kreis k1 = new Kreis(0,0,9);
		
		System.out.printf("Rechteck Area= %.2f\n" , r1.determineArea());
		System.out.printf("Kreis Area= %.2f\n" , k1.determineArea());

	}

}
