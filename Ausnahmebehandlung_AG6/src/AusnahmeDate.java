
public class AusnahmeDate extends Exception {

	private int fdate;

	public AusnahmeDate(String msg, int fdate) {
		super(msg);
		this.fdate = fdate;
	}

	public int getFDate() {
		return this.fdate;
	}
}
