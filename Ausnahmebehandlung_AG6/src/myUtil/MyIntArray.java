package myUtil;

import java.util.Arrays;

public class MyIntArray implements IIntArray {

	private int[] array;
	
	public MyIntArray() {
		array = new int[0];
	}
	
	public int get(int index) throws MyIndexException{

	if(index <0 || index >array.length - 1) {
		throw new MyIndexException(index);
	}
	return array [index];
	}
	
	public 	void set(int index, int value) throws MyIndexException{
		if (index <0 || index > array.length - 1) {
			throw new MyIndexException (index);
		}
		
		array[index] = value; 
	}
	
	public void increase(int n) {
		if (n < 0) {
			n = 0;
		}
		int[] Length = new int[this.array.length + n];
		for (int i = 0; i < array.length; i++) {
			Length[i] = array[i];
		}
		array = Length;
	}

	@Override
	public String toString() {
		return "MyIntArray [array=" + Arrays.toString(array) + "]";
	}

}
