package myUtil;

public class ArrayTest {

	public static void main(String[] args) {

		MyIntArray myObj = new MyIntArray();
		myObj.increase(5);
		
		try {
			for (int i = 0; i < 5; i++) {
				myObj.set(i, i + 1);
			}
		}catch (MyIndexException ex) {
			System.out.println(ex + "Wrong index! " + ex.getWrongIndex());
		}
		
		
		try {
		System.out.println(myObj.get(10));
			
		}catch (MyIndexException ex) {
			System.out.println(ex + "\tWrong index! " + ex.getWrongIndex());
		}
		
		System.out.println(myObj);

	}

}
