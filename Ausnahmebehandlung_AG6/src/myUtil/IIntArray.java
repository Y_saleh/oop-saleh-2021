package myUtil;
public interface IIntArray {
	/**
	 * Funktion liefert den Wert am angegebenen Index eines Arrays
	 * @param index der Index
	 * @return der Wert, der an index steht
	 * @throws MyIndexException, wenn index ungltig ist
	 */
	int get(int index) throws MyIndexException;
	
	/**
	 * Funktion setzt einen bestimmten Wert am angegebenen Index eines Arrays
	 * @param index der Index, dessen Wert gesetzt werden soll
	 * @param value der neue Wert
	 * @throws MyIndexException, wenn index ungltig ist
	 */
	void set(int index, int value) throws MyIndexException;
	

	void increase(int n);
}