
public class Date {

	private int day;
	private int month;
	private int year;
	private static int anzahl = 0;

	public Date() {
		this.day = 1;
		this.month = 1;
		this.year = 1970;
		anzahl++;
	}

	public Date(int day, int month, int year) throws AusnahmeDay, AusnahmeMonth, AusnahmeYear {
		setDay(this.day);
		setMonth(this.month);
		setYear(this.year);
//		anzahl++;
	}

	public static int getAnzahl() {
		return anzahl;
	}

	public int getDay() {
		return day;
	}

	public int getMonth() {
		return month;
	}

	public int getYear() {
		return year;
	}

	public void setDay(int day) throws AusnahmeDay {
		if (day < 1 || day > 31)
			throw new AusnahmeDay("Wrong Day input", day);

		this.day = day;
	}

	public void setMonth(int month) throws AusnahmeMonth {
		if (month < 1 || month > 12)
			throw new AusnahmeMonth("Wrong Month input", month);

		this.month = month;
	}

	public void setYear(int year) throws AusnahmeYear {
		if (year < 1900 || year > 2100)
			throw new AusnahmeYear("Wrong Year input", year);

		this.year = year;
	}

	public String toString() {

		return " " + this.day + " . " + this.month + " . " + this.year;
	}

}
