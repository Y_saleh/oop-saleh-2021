import java.util.Scanner;  // Import the Scanner class

public class Division {

	public static void main(String[] args) {
	
		Scanner myObj = new Scanner(System.in);  // Create a Scanner object

		System.out.println("Bitte geben Sie den Wert X:");
	    double x = myObj.nextDouble();
		System.out.println("Bitte geben Sie den Wert Y:");
		double y = myObj.nextDouble();
	    try {
	    	div0(x,y);
	    } catch (ArithmeticException ex) {
			System.out.println("Error: " + ex.getMessage() + "!");
	    }
		
	}
	
	public static void div0(double x, double y) throws ArithmeticException{
		if (y == 0) {
			throw new ArithmeticException("Division durch Null");
		}
		
		System.out.print("X / Y = ");
		double z = x/y; 
		System.out.print(z + ".");
	}

}
