import java.util.Scanner; // Import the Scanner class

public class DateTest {

	public static void main(String[] args) throws AusnahmeDay, AusnahmeMonth, AusnahmeYear {
		Scanner myObj = new Scanner(System.in); // Create a Scanner object
		boolean dayError = false;
		boolean monthError = false;
		boolean yearError = false;

		Date d1 = new Date();

		System.out.println("Please write the Date (DD/MM/YYYY):");

		try {
			d1.setDay(myObj.nextInt());
			System.out.println("/");
			d1.setMonth(myObj.nextInt());
			System.out.println("/");
			d1.setYear(myObj.nextInt());

		} catch (AusnahmeDay ex) {
		 dayError = true;
			System.out.println("Error: " + ex.getMessage() + "! " + ex.getFDate());
		} catch (AusnahmeMonth ex) {
			 monthError = true;
			System.out.println("Error: " + ex.getMessage() + "! " + ex.getFDate());
		} catch (AusnahmeYear ex) {
			 yearError = true;
			System.out.println("Error: " + ex.getMessage() + "! " + ex.getFDate());
		}
		
		if (dayError == false && monthError == false && yearError == false) {
			System.out.println("No Errors, You're Good:)");
		}
		

	}

}