﻿import java.util.Scanner;


class Fahrkartenautomat_7
{
    public static void main(String[] args)
    {      

        
       double zuZahlenderBetrag = 0; 
       int ticketAnzahl = 0;
       double eingezahlterGesamtbetrag = 0;
       double eingeworfeneMünze = 0;
       int Intrückgabebetrag;
        
       	fahrkartenbesellvorgang("Fahrkartenbestellvorgang:");
       	//Warte(20);
       	//fahrkartenbesellvorgang("/tEinzelfahrschen Regeltarif AB [2,90 \u20AC] (1)");



		zuZahlenderBetrag = fahrkartenbestellungErfassen("Wahlen Sie ihre Wunschfahrkarte fur Berlin AB aus:");//ticketTyp
		ticketAnzahl = fahrkartenAnzahl("Anzahl der Tickets: ");//
		zuZahlenderBetrag *= ticketAnzahl;
		 Intrückgabebetrag = fahrkartenBezahlen(eingezahlterGesamtbetrag , zuZahlenderBetrag,  eingeworfeneMünze);
		 fahrkartenAusgeben("\nFahrschein wird ausgegeben");
		// Warte(250); //Warte funktion, in int millisekunde
         rueckgeldAusgeben(Intrückgabebetrag);
       
       System.out.println("\nVeergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
       Warte(10000);
       clearScreen();
       main(args);
    }
    
 	public static String fahrkartenbesellvorgang(String text) {

     System.out.println(text);
     System.out.println("\n\n");
     return null; 
 	}
    
    public static double fahrkartenbestellungErfassen(String text) {
	    Scanner myScanner = new Scanner(System.in);

    	
    	String[] kartename = {"","Einzelfahrschein Berlin AB         ", "Einzelfahrschein Berlin BC             ","Einzelfahrschein Berlin ABC          "
    							,"Kurzstrecke                        ", "Tageskarte Berlin AB                   ", "Tageskarte Berlin BC               "
    							,"Tageskarte Berlin ABC              ", "Kleingruppen-Tageskarte Berlin AB      ", "Kleingruppen-Tageskarte Berlin BC   "
    							,"Kleingruppen-Tageskarte Berlin ABC "};
    	double[] kartenpreis = { 0, 2.9, 3.3, 3.6, 1.9, 8.6, 9.0, 9.6, 23.5, 24.3, 24.9} ;
 
        double Betrag = 0;
        boolean condition = false;
        if (kartename.length != kartenpreis.length) {
				System.out.printf("error: Kartenname Array[%d] != Kartenpreis[%d]. Bitte den Code Uberprufen", kartename.length, kartenpreis.length);
        System.exit(0);
    	}
		System.out.println(text);
		for (int i = 1; i < kartename.length; i++) {

			   System.out.printf("\t" + kartename[i]);
			   				System.out.printf("\t\t[%.2f \u20AC]", kartenpreis[i]);
			   						System.out.printf("\t(%d)\n", (i));
			}
		
		
		while (condition == false) {	
		System.out.printf("\n\n\nIhre Wahl:");
		int x = myScanner.nextInt();
		
		if (x < kartename.length && x > 0) {
		    condition = true;
	    System.out.printf("Sie Haben die karte %S ausgewahlt.\n", kartename[x]);
	    System.out.printf("Dieses Kostet: %.2f \u20AC\n\n", kartenpreis[x]);
	    Betrag = kartenpreis[x];
		} else {
		    System.out.println("\n\n\t\t>>falsche Eingabe<<");
		    
		}
		}	   
		   return Betrag;
		   }
    
    public static int fahrkartenAnzahl(String text) {
		   Scanner myScanner = new Scanner(System.in);
		   int ticketAnzahl = 0;
	
			while (true) {
				System.out.println(text);
				ticketAnzahl = myScanner.nextInt();
				
				if (ticketAnzahl > 10 || ticketAnzahl <= 0) {
					System.out.println("\nError: Bitte geben Sie eine Ticket Anzahl von 1 bis 10 ");
					continue;
			}  
					break;			
			}
		   return ticketAnzahl;
		   } 
    
    public static int fahrkartenBezahlen(double eingezahlterGesamtbetrag ,double zuZahlenderBetrag, double eingeworfeneMünze) {
    	Scanner myScanner = new Scanner(System.in);
    	
    	 eingezahlterGesamtbetrag = 0.0;
    	    while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
    	    {
    	    	muenzeAusgeben((zuZahlenderBetrag - eingezahlterGesamtbetrag), "euro");
    	 	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 \u20AC): ");
    	 	   eingeworfeneMünze = myScanner.nextDouble();
    	        eingezahlterGesamtbetrag += eingeworfeneMünze;
    	    }  
            int Intrückgabebetrag = (int)(100*(eingezahlterGesamtbetrag - zuZahlenderBetrag));

		   return Intrückgabebetrag;
		   }
    
	public static String fahrkartenAusgeben(String text) {

    System.out.println(text);
    System.out.println("\n\n");
    
    for (int i = 0; i < 8; i++)
    {
       System.out.print("=");
       Warte(250);
    }
    
    System.out.println("\n\n");
    return null; 
	}
	
    public static void Warte(int millisekunde) {
    	try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
    
    }	
    
    public static void clearScreen() {  
    	for(int clear = 0; clear < 15; clear++)
    	  {
    	     System.out.println(" ") ;
    	  } 
    } 
    
    public static void muenzeAusgeben(double betrag, String einheit) {
    	System.out.printf("Noch zu zahlen: %.2f", betrag);
    	
    	if (einheit == "euro")
    	System.out.printf("\u20AC\n");
    	else if (einheit == "dollar")
    	System.out.printf("\40$\n");
    	else
        	System.out.printf("--");

    	//System.out.printf("Noch zu zahlen: %.2f \u20AC\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
    }
	public static int rueckgeldAusgeben(int Intrückgabebetrag) {
		double rückgabebetrag = 0;

	if( Intrückgabebetrag > 0.0)
    {
 	   rückgabebetrag = (double)(Intrückgabebetrag); 
 	   rückgabebetrag /= 100;
 	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f \u20AC\n", rückgabebetrag);
 	   System.out.println("wird in folgenden Münzen ausgezahlt:");

        while(Intrückgabebetrag >= 200) // 2 EURO-Münzen
        {
     	  System.out.println("2 EURO");
     	  Intrückgabebetrag -= 200;
        }
        while(Intrückgabebetrag >= 100) // 1 EURO-Münzen
        {
     	  System.out.println("1 EURO");
     	  Intrückgabebetrag -= 100;
        }
        while(Intrückgabebetrag >= 50) // 50 CENT-Münzen
        {
     	  System.out.println("50 CENT");
     	  Intrückgabebetrag -= 50;
        }
        while(Intrückgabebetrag >= 20) // 20 CENT-Münzen
        {
     	  System.out.println("20 CENT");
     	  Intrückgabebetrag -= 20;
        }
        while(Intrückgabebetrag >= 10) // 10 CENT-Münzen
        {
     	  System.out.println("10 CENT");
     	  Intrückgabebetrag -= 10;
        }
        while(Intrückgabebetrag >=5)// 5 CENT-Münzen
        {
     	  System.out.println("5 CENT");
     	  Intrückgabebetrag -= 5;
        }
    	}
	return 0;
	}
}