
public class Teilnehmer extends Person {

		private String status;
		
		public Teilnehmer () {
//			setName("Unbekannt");
//			setAlter(0);
			super();
			this.status = "---";
		}
		
		public Teilnehmer (String name, int alter, String status) {
			super(name, alter);
			setStatus(status);
		
		}

		
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status; 
		}
		
		@Override
		public String toString() {
			return "Name: " + getName() + " , Alter: " + getAlter() + " , Status: " + this.status;
		}
}
