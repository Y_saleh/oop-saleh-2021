
public class Person {

	private String name;
	private int alter; 
	
	public Person() {
		this.name  = "Unbekannt";
		this.alter = 0;
	}
	public Person (String name, int alter) {
		this.name  = name;
		this.alter = alter;
	}
	public String getName() {
		return name;
	}
	public int getAlter() {
		return alter;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setAlter(int alter) {
		this.alter = alter;
	}
	public String toString() {    // {Name: Max, Alter: 18}
	
		return "[ Name: " + this.name + " , Alter: " + this.alter + " ]";
	}
	
	public boolean equals(Object obj) {
		//if (obj == null)
		// return false;
		
	if  (obj instanceof Person) {
		Person p = (Person) obj;
		if(this.name.equals(p.getName()) &&
			this.alter == p.getAlter())
			return true;
		else
			return false;
	}
	return false;
}
}
