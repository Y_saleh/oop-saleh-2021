
public class Date {

	private int day;
	private int month;
	private int year;
	private static int anzahl = 0;

	public Date() {
		this.day = 1;
		this.month = 1;
		this.year = 1970;
		anzahl++;
	}

	public Date(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
		anzahl++;
	}

	public static int getAnzahl() {
		return anzahl;
	}

	public int getDay() {
		return day;
	}

	public int getMonth() {
		return month;
	}

	public int getYear() {
		return year;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public void setYear(int year) {
		this.year = year;
	}

	@Override
	public String toString() {

		return " " + this.day + " . " + this.month + " . " + this.year ;
	}

}
